package com.company;

import java.util.ArrayList;
import java.util.List;

public class Sorteio {
    private Dado dado;
    private List<Integer> lances;
    private int resultadoAgregado;
    private int quantidadeDeLances;

    public Sorteio(int quantidadeDeLances, int ladosDoDado) {
        this.resultadoAgregado = 0;
        this.quantidadeDeLances = quantidadeDeLances;
        lances = new ArrayList<>();
        dado = new Dado(ladosDoDado);
    }

    public void setDado(Dado dado) {
        this.dado = dado;
    }

    public Dado getDado() {
        return dado;
    }

    public List<Integer> getLances() {
        return lances;
    }

    public void setLances(List<Integer> lances) {
        this.lances = lances;
    }

    public int getResultadoAgregado() {
        return resultadoAgregado;
    }

    public void setResultadoAgregado(int resultadoAgregado) {
        this.resultadoAgregado = resultadoAgregado;
    }

    public void sortear(){
        for (int i = 0; i < quantidadeDeLances; i++) {
            lances.add(dado.lancar());
            resultadoAgregado+= lances.get(i);
        }
    }
}
