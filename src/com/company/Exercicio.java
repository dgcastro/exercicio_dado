package com.company;

import java.util.ArrayList;
import java.util.List;

public class Exercicio {
    private List<Sorteio> sorteios;
    private int quantidadeDeSorteios;

    public Exercicio() {
    }

    public Exercicio(int quantidadeDeSorteios) {
        this.quantidadeDeSorteios = quantidadeDeSorteios;
        sorteios = new ArrayList<>();
    }

    public void executar() {
        for (int i = 0; i < quantidadeDeSorteios; i++) {
            sorteios.add(new Sorteio(3, 6));
            sorteios.get(i).sortear();
            System.out.println("Sorteio (" + (i + 1) + ")");
            for (int j = 0; j < sorteios.get(i).getLances().size(); j++) {
                System.out.print(sorteios.get(i).getLances().get(j) + ", ");
            }
            System.out.println(sorteios.get(i).getResultadoAgregado());
        }
    }
}
