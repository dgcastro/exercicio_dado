package com.company;

import java.util.Random;

public class Dado {
    private int lados;

    public Dado() {
    }

    public Dado(int lados){
        this.lados = lados;
    }

    public int getLados() {
        return lados;
    }

    public void setLados(int lados) {
        this.lados = lados;
    }

    public int lancar(){
        return new Random().nextInt(lados)+1;
    }
}
